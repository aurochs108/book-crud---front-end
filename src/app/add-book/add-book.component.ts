import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Book } from '../book';
import { BookService } from '../book.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  @Output() refreshTable = new EventEmitter();
  titleInputValue = '';
  authorInputValue = '';
  pagesInputValue = '';

  constructor(
    private bookService: BookService,
  ) {
  }

  ngOnInit() {
  }

  addBook(form) {
    const todayLocalDate = new Date().toISOString().split('T')[0];

    const bookFromSubmit: Book = {
      title: form.value.title,
      author: form.value.author,
      pages: form.value.pages,
      yearOfPublication: todayLocalDate,
      id: null
    };

    this.bookService.addBook(bookFromSubmit);
    this.titleInputValue = '';
    this.authorInputValue = '';
    this.pagesInputValue = '';
    this.refreshTable.emit();
  }
}


