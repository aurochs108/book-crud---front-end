export class Book {
  id: number;
  title: string;
  author: string;
  yearOfPublication: string;
  pages: number;
}
