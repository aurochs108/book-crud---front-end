import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { Book } from '../book';
import { BookService } from '../book.service';
import { AuthService } from '../service/auth/auth.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['/table.component.css']
})
export class TableComponent implements OnInit {
  books: Book[];
  subIsTableActual: Subscription;
  private lastDateOfTableUpdate: Date;

  headElements = ['ID'
    , 'Title'
    , 'Author'
    , 'Pages'
    , 'Release Date'
    , ''
    , ''
  ];

  constructor(
    private bookService: BookService,
    private authService: AuthService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.getBooks();
  }

  getBooks(): void {
    this.bookService.getBooks()
      .subscribe(books => this.books = books);
  }

  deleteBook(bookId: number): void {
    this.books.filter(book => book.id !== bookId);
    this.bookService.deleteBook(bookId).subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log(error);
      },
      () => this.getBooks()
    );
  }

  refreshTable(): void {
    this.getBooks();
  }

  updateBook(book: Book): void {
    console.log(book);
  }
}
