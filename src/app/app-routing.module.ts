import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { TableComponent } from './table/table.component';
import { AddBookComponent } from './add-book/add-book.component';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'table', component: TableComponent},
  {path: 'addBook', component: AddBookComponent},
  {path: '', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
