import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Book } from './book';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private bookUrl = 'http://localhost:8080/books';

  constructor(
    private http: HttpClient
  ) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  getBooks(): Observable<Book[]> {
    return this.http
      .get<Book[]>(this.bookUrl + '/getAll');

  }

  deleteBook(bookId: number) {
    const deleteUrl = this.bookUrl + `/deleteBook/` + bookId;

    return this.http.delete(deleteUrl);
  }

  async addBook(book: Book) {
    const postUrl = this.bookUrl + `/saveBook`;
    this.http.post(postUrl, book).subscribe();
  }

}
